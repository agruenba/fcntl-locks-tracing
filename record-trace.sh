#! /bin/sh

set -e

run() {
    echo "$*"
    "$@"
}

run perf record \
	-e probe:fcntl_getlk \
	-e probe:fcntl_getlk_ret \
	-e probe:vfs_lock_file \
	-e probe:fcntl_setlk__return
