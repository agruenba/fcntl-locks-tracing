# RHEL-8 fcntl(2) F_GETLK/F_SETLK/F_SETLKW tracing

This repository contains scripts for setting up and recording perf kernel trace
points and a perf script for computing the number of locking requests per inode
per second.

The trace points are attached to functions fcntl_getlk, vfs_lock_file, and
fcntl_setlk in the kernel in the vfs part of the code, so they should work for
any filesystem.  They record the beginning and end of fcntl(2) locking
requests.

The trace points have been tested with RHEL-8 kernel version
4.18.0-477.3.1.el8.x86_64, and they should work on kernels close to that
version.  For much older or newer kernels, the trace points will require
adjustments.

## Prerequisites

* Install matching `kernel` and `kernel-debuginfo` packages.  For more information on kernel debuginfo packages, see [How can I download or install kernel debuginfo packages for RHEL systems](https://access.redhat.com/solutions/9907).
* Install the `perf` package.

## Tracing

* Run the `prepare-trace.sh` for setting up the fcntl(2) trace points.
* Run the `record-trace.sh` script for recording the fcntl(2) trace points.
* Run the workload to trace.
* Interrupt the `record-trace.sh` script with Ctrl-C or similar.

This will leave around a `perf.data` file in the current working directory.

## Analyzing the trace data

The raw trace events recorded in in `perf.data` can be inspected using the `perf script` command.

Use the `perf-script.py` script for summarizing the trace data by counting the number of get, lock, and unlock requests per inode per second, as follows:

```
perf script [-i perf.data] perf-script.py [--dev=device]
```

The `perf-script.py` script can only summarize the perf events of a single
filesystem, identified by the device block number.  In case `perf.data`
contains trace events from multiple block devices, use the `--dev=NUMBER`
option to select the device to look at.  The NUMBER to use is the device
major/minor number in decimal; check the block special file under `/dev/` or
have a look at the output of `perf script` to figure out which number to use.

The output of `perf-script.py` will be a table of locking operation
frequencies, with a header line each for the inode numbers and operations, and
a further line for each second in which trace events have been recorded.  Each
line records the second and the number of get, lock, and unlock requests that
were recorded that second.

This table can easily be viewed as a spreadsheet.
