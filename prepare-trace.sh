#! /bin/sh

unprobe() {
    for probe in "$@"; do
	perf probe --quiet --del "$probe" || :
    done
}

probe() {
    if ! perf probe --quiet "$@"; then
	echo perf probe "$@"
	perf probe "$@" || exit $?
    fi
}

unprobe probe:fcntl_getlk
probe --add='fcntl_getlk=fcntl_getlk:26
	dev=filp->f_inode->i_sb->s_dev:u
	ino=filp->f_inode->i_ino:u
	pid=fl->fl_pid:u
	owner=fl->fl_owner:u
	type=fl->fl_type:u
	flags=fl->fl_flags:u
	start=fl->fl_start:u
	end=fl->fl_end:u'

unprobe probe:fcntl_getlk_ret
probe --add='fcntl_getlk_ret=fcntl_getlk:27
	type=fl->fl_type:u
	flags=fl->fl_flags:u
	start=fl->fl_start:u
	end=fl->fl_end:u
	pid=fl->fl_pid:u'

unprobe vfs_lock_file
probe --add='vfs_lock_file
	dev=filp->f_inode->i_sb->s_dev:u
	ino=filp->f_inode->i_ino:u
	cmd=cmd:u
	pid=fl->fl_pid:u
	owner=fl->fl_owner:u
	type=fl->fl_type:u
	flags=fl->fl_flags:u
	start=fl->fl_start:u
	end=fl->fl_end:u'

unprobe probe:fcntl_setlk__return
probe --add='fcntl_setlk=fcntl_setlk%return
	rv=$retval'
